
var game = new Phaser.Game(800, 600, Phaser.Auto, '',
 { preload: preload, create: create, update: update, render: render });





function preload () {
    //game.load.image('Player','p1_front.png');
    game.load.image('GrassMid','grassMid.png');
    game.load.spritesheet('Player', 'p1_walk.png', 69, 92);
    game.load.image('Platform','Platform.png');
    game.load.image('PartialPlatform','PartialPlatform.png');
    game.load.image('Background','bg.png');
    game.load.image('RedGem','gemRed.png');
    game.load.image('GreenGem','gemGreen.png');
    game.load.image('BlueGem','gemBlue.png');
    game.load.image('FixedPlatform','FixedPlatform.png');
    game.load.image('Cloud','cloud2.png');
    game.load.spritesheet('Title','Title2.png',50,60);
    game.load.image('Subtitle','Subtitle.png');
    game.load.image('Key','keyYellow.png');

    game.load.image('Heart','hud_heartFull.png');
    game.load.image('HeartEmpty','hud_heartEmpty.png');      
    game.load.image('Water','liquidWaterTop.png');
    game.load.audio('Theme', 'MonkeysSpinningMonkeys.mp3');

}




/*Code for the coloured platforms */ 
function Platform(x,y,colour,name,type)
{
    this.x = x;
    this.y = y;
    this.OrigX = x;
    this.OrigY = y;
    this.colour = colour;

    if( type == 0 )
    {
        this.AddSolidSprite(name);
        this.spriteShown = name;
    }
    else
    {
        this.AddNormalSprite(name);
        this.spriteNormal = name;
    }
}

Platform.prototype.AddSolidSprite = function(name)
{
    //kill(this.sprite);
    this.sprite = soildEntities.create(this.x, this.y, name);


    if( this.colour == 0)
        this.sprite.tint  = 0xFF0000;
    else if (this.colour == 1)
        this.sprite.tint  = 0x00FF00;
    else if (this.colour == 2)
        this.sprite.tint  = 0x0000FF;


    this.sprite.body.immovable     = true;
}

Platform.prototype.AddNormalSprite = function(name,colour)
{
    this.sprite = game.add.sprite(this.x, this.y, name);
    if( this.colour == 0)
        this.sprite.tint  = 0xFF0000;
    else if (this.colour == 1)
        this.sprite.tint  = 0x00FF00;
    else if (this.colour == 2)
        this.sprite.tint  = 0x0000FF;
}
Platform.prototype.SetVelocity = function(dx,dy,amountX,amountY)
{
    this.dx = dx;
    this.dy = dy;
    this.amountX = amountX;
    this.amountY = amountY;
}




Platform.prototype.Update = function(){}

function Halt()
{
    this.sprite.body.velocity.x = 0;
    this.sprite.body.velocity.y = 0;   
}

function Start()
{
    this.sprite.body.velocity.x = this.dx;
    this.sprite.body.velocity.y = this.dy;
}
function Show()
{
    this.sprite.kill();
    this.AddSolidSprite(this.spriteShown);
}
function Hide()
{
    this.sprite.kill();
    this.AddNormalSprite(this.spriteNormal);
}

function Move()
{
    if( this.OrigX + this.amountX < this.x || this.OrigX - this.amountX > this.x )
    {
        this.dx = -this.dx;
        this.sprite.body.velocity.x = this.dx;
    }   

    if( this.OrigY + this.amountY < this.y || this.OrigY - this.amountY > this.y )
    {
        this.dy = -this.dy;
        this.sprite.body.velocity.y = this.dy;
    }   
    this.x += this.dx;
    this.y += this.dy;
}

/* End of platform code */



var background;
var player;
var platforms;
var gems;
//The ground & platform
var soildEntities;

//The water which kills the player upon collision
var water;
var introPlaying;

var levels;
var levelIndex;
var key;

var health;
var healthSprites;


var playerDead  = false;
function MoveCloud()
{
    if( this.x + this.width < game.camera.x)
    {
        this.x = game.camera.x + game.camera.view.width + (Math.random()*200); // Move x position
        this.y = (Math.random()*(game.camera.view.height/2));
    }

    this.x -= 1;
}


function create () {
    game.physics.startSystem(Phaser.Physics.ARCADE);
    game.world.setBounds(0, 0, 800*4, 600);
   

    var theme = game.add.audio('Theme',1,true);

    theme.play('',0,1,true);

    health = 3;
    levelIndex = 0;   
    levels = [CreateMainMenu,CreateLevelOne,CreateLevelTwo,CreateLevelThree,CreateLevelFour,CreateLevelFive];
    levels[levelIndex]();
}


/* Level creation helper functions */

function AddBackground()
{
    background    = game.add.sprite(0,0,'Background');
    background.scale.x = 4;
    background.scale.y = 4;
    background.fixedToCamera = true;

    var cloud1 = game.add.sprite(-300,300,'Cloud');
    cloud1.update = MoveCloud;
    var cloud2 = game.add.sprite(-300,300,'Cloud');
    cloud2.update = MoveCloud;
    var cloud3 = game.add.sprite(-300,300,'Cloud');
    cloud3.update = MoveCloud;
    var cloud4 = game.add.sprite(-300,300,'Cloud');
    cloud4.update = MoveCloud;
}

function AddPlayer(x,y)
{

    player = game.add.sprite(x,y,'Player');
    player.animations.add('walking',[0,1,2,3,4,5,6,7,8,9,10],20,true);  
    game.physics.arcade.enable(player);
    player.body.gravity.y = 500;
    player.anchor.setTo(0.5, 1.0);
    game.camera.follow(player);    

}

function AddHealthHud()
{
   healthSprites = new Array(3);
    for(var index = 0; index < 3; ++index)
    {
        if( index < health )
        {
            healthSprites[index] = game.add.sprite(740 - index*60,50,'Heart');
        }
        else
        {
            healthSprites[index] = game.add.sprite(740 - index*60,50,'HeartEmpty');
        }   
        healthSprites[index].fixedToCamera = true;
    }
}

function CreateSubtitle()
{
    game.add.sprite(200,game.world.centerY,'Subtitle');
    introPlaying = false;
}



/* Level creation functions */
function CreateMainMenu()
{
    AddBackground();
    UpdateMask = MainMenuUpdate;
    game.camera.reset();

    introPlaying = true;
    for(var i  =0; i < 9; ++i)
    {
        var letter = game.add.sprite(180 + i*50,-100,'Title',i);

        var tween = game.add.tween(letter);
        tween.to({y: game.world.centerY-100}, 0, Phaser.Easing.Bounce.Out, true, 300 * i, false);

        if( i == 8 )
            tween.onComplete.add(CreateSubtitle,this);

    }

}


function CreateLevelOne()
{
    AddBackground();
    AddHealthHud();

    soildEntities = game.add.group();
    soildEntities.enableBody = true;

    water = game.add.group();
    water.enableBody = true;

    for (var i = 0; i < 24; i++)
    {

        var ground;
        if( i > 4 &&i < 15 )
            ground = soildEntities.create(i*70, 530, 'GrassMid');
        else 
            ground = water.create(i*70,530,'Water');
        ground.body.immovable = true;
    }

    var p1 = new Platform(600,450,0,'PartialPlatform',1);
    p1.ActivationFunction  = Show;
    p1.UpdateReplace       = Platform.prototype.Update;
    p1.DeacticationFunction= Hide;
    p1.spriteShown         = 'Platform';
    platforms = [p1];

    var fixedPlat = soildEntities.create(800, 300, 'FixedPlatform');
    fixedPlat.body.immovable = true;

    gems = game.add.group();
    gems.enableBody = true;

    var g = gems.create(900, 400, 'RedGem');
    g.colour = 0;




    AddPlayer(400,50);

    key = game.add.sprite(880,230,'Key');
    game.physics.arcade.enable(key);
    key.immovable = false;


    UpdateMask = MainGameUpdate;
}


function CreateLevelTwo()
{
    AddBackground();

    AddHealthHud();

    soildEntities = game.add.group();
    soildEntities.enableBody = true;
    water = game.add.group();
    water.enableBody = true;

    for (var i = 0; i < 30; i++)
    {

        var ground;
        if( i > 3 &&i < 10 )
            ground = soildEntities.create(i*70, 530, 'GrassMid');
        else 
            ground = water.create(i*70,530,'Water');
        ground.body.immovable = true;
    }

    var p2= new Platform(800,400,0,'Platform',0);
    p2.SetVelocity(100,0,5250,0); 
    p2.ActivationFunction  = Start;
    p2.UpdateReplace       = Move;
    p2.DeacticationFunction= Halt;

    platforms = [p2];

    var fixedPlat = soildEntities.create(1200, 400, 'FixedPlatform');
    fixedPlat.body.immovable = true;

    gems = game.add.group();

    gems.enableBody = true;

    var g = gems.create(600, 400, 'RedGem');
    g.colour = 0;



    AddPlayer(200,50);


    key = game.add.sprite(1200,330,'Key');
    game.physics.arcade.enable(key);
    key.immovable = false;


    UpdateMask = MainGameUpdate;
}


function CreateLevelThree()
{
    AddBackground();

    AddHealthHud();

    soildEntities = game.add.group();
    soildEntities.enableBody = true;
    water = game.add.group();
    water.enableBody = true;

    for (var i = 0; i < 35; i++)
    {
        var ground;
        if( i > 3 &&i < 10 )
            ground = soildEntities.create(i*70, 530, 'GrassMid');
        else 
            ground = water.create(i*70,530,'Water');
        ground.body.immovable = true;
    }



    var p1 = new Platform(800,400,0,'PartialPlatform',1);
    p1.ActivationFunction  = Show;
    p1.UpdateReplace       = Platform.prototype.Update;
    p1.DeacticationFunction= Hide;
    p1.spriteShown         = 'Platform';

    var p2 = new Platform(1400,400,1,'PartialPlatform',1);
    p2.ActivationFunction  = Show;
    p2.UpdateReplace       = Platform.prototype.Update;
    p2.DeacticationFunction= Hide;
    p2.spriteShown         = 'Platform';

    platforms = [p1,p2];

    var fixedPlat = soildEntities.create(1100, 400, 'FixedPlatform');
    fixedPlat.body.immovable = true;

    fixedPlat = soildEntities.create(1700, 400, 'FixedPlatform');
    fixedPlat.body.immovable = true;

    gems = game.add.group();

    gems.enableBody = true;

    var g = gems.create(600, 400, 'RedGem');
    g.colour = 0;


    var g = gems.create(1300, 330, 'GreenGem');
    g.colour = 1;


    AddPlayer(400,50);


    key = game.add.sprite(1700,330,'Key');
    game.physics.arcade.enable(key);
    key.immovable = false;


    UpdateMask = MainGameUpdate;
}



function CreateLevelFour()
{
    AddBackground();

    AddHealthHud();

    soildEntities = game.add.group();
    soildEntities.enableBody = true;
    water = game.add.group();
    water.enableBody = true;

    for (var i = 0; i < 50; i++)
    {
        var ground;
        if( i > 3 &&i < 10 )
            ground = soildEntities.create(i*70, 530, 'GrassMid');
        else 
            ground = water.create(i*70,530,'Water');
        ground.body.immovable = true;
    }



    var p1 = new Platform(800,400,0,'PartialPlatform',1);
    p1.ActivationFunction  = Show;
    p1.UpdateReplace       = Platform.prototype.Update;
    p1.DeacticationFunction= Hide;
    p1.spriteShown         = 'Platform';

    var p2 = new Platform(1300,400,1,'PartialPlatform',1);
    p2.ActivationFunction  = Show;
    p2.UpdateReplace       = Platform.prototype.Update;
    p2.DeacticationFunction= Hide;
    p2.spriteShown         = 'Platform';
    
    var p3 = new Platform(1800,400,0,'PartialPlatform',1);
    p3.ActivationFunction  = Show;
    p3.UpdateReplace       = Platform.prototype.Update;
    p3.DeacticationFunction= Hide;
    p3.spriteShown         = 'Platform';
    
    var p4 = new Platform(2200,400,1,'PartialPlatform',1);
    p4.ActivationFunction  = Show;
    p4.UpdateReplace       = Platform.prototype.Update;
    p4.DeacticationFunction= Hide;
    p4.spriteShown         = 'Platform';

    var p5 = new Platform(2700,400,0,'PartialPlatform',1);
    p5.ActivationFunction  = Show;
    p5.UpdateReplace       = Platform.prototype.Update;
    p5.DeacticationFunction= Hide;
    p5.spriteShown         = 'Platform';

    platforms = [p1,p2,p3,p4,p5];


    gems = game.add.group();
    gems.enableBody = true;

    var g = gems.create(600, 400, 'RedGem');
    g.colour = 0;


    var g = gems.create(1100, 200, 'GreenGem');
    g.colour = 1;

    var g = gems.create(1600, 200, 'RedGem');
    g.colour = 0;
   
    var g = gems.create(2000, 200, 'GreenGem');
    g.colour = 1;

    var g = gems.create(2500, 200, 'RedGem');
    g.colour = 0;


    AddPlayer(400,50);


    key = game.add.sprite(2800,330,'Key');
    game.physics.arcade.enable(key);
    key.immovable = false;


    UpdateMask = MainGameUpdate;
}


function CreateLevelFive()
{
    AddBackground();

    AddHealthHud();

    soildEntities = game.add.group();
    soildEntities.enableBody = true;
    water = game.add.group();
    water.enableBody = true;

    for (var i = 0; i < 50; i++)
    {
        var ground;
        if( i > 3 &&i < 10 )
            ground = soildEntities.create(i*70, 530, 'GrassMid');
        else 
            ground = water.create(i*70,530,'Water');
        ground.body.immovable = true;
    }



    var p1 = new Platform(900,300,0,'Platform',0);
    p1.SetVelocity(100,-100,5250,5250); 
    p1.ActivationFunction  = Start;
    p1.UpdateReplace       = Move;
    p1.DeacticationFunction= Halt;

    var p2 = new Platform(2100,500,2,'Platform',0);
    p2.SetVelocity(100,0,5250,0); 
    p2.ActivationFunction  = Start;
    p2.UpdateReplace       = Move;
    p2.DeacticationFunction= Halt;
    

    var p3 = new Platform(2700,450,0,'PartialPlatform',1);
    p3.ActivationFunction  = Show;
    p3.UpdateReplace       = Platform.prototype.Update;
    p3.DeacticationFunction= Hide;
    p3.spriteShown         = 'Platform';
    

    platforms = [p1,p2,p3];


    var fixedPlat = soildEntities.create(1500, 500, 'FixedPlatform');
    fixedPlat.body.immovable = true;
    var fixedPlat = soildEntities.create(1710, 500, 'FixedPlatform');
    fixedPlat.body.immovable = true;
    var fixedPlat = soildEntities.create(2900, 450, 'FixedPlatform');
    fixedPlat.body.immovable = true;

    gems = game.add.group();
    gems.enableBody = true;

    var g = gems.create(600, 400, 'RedGem');
    g.colour = 0;

    var g = gems.create(1710, 435, 'BlueGem');
    g.colour = 2;

    var g = gems.create(1950, 435, 'GreenGem');
    g.colour = 1;

    var g = gems.create(2500, 435, 'GreenGem');
    g.colour = 1;

    var g = gems.create(2650, 400, 'RedGem');
    g.colour = 0;


    AddPlayer(400,50);


    key = game.add.sprite(3000,330,'Key');
    game.physics.arcade.enable(key);
    key.immovable = false;


    UpdateMask = MainGameUpdate;
}



function UpdateMask(){}
function update()
{
    UpdateMask();
}

function MainMenuUpdate()
{
    if( !introPlaying && game.input.mousePointer.isDown)
    {
        levels[++levelIndex]();

    }

}


function MainGameUpdate () {

    
    //Check for collisions between the player and all solid entities
    game.physics.arcade.collide(player, soildEntities);



    var numPlatforms = platforms.length;

    for(var index = 0; index < numPlatforms; ++index)
    {
        platforms[index].Update();
    }


	if (game.input.keyboard.isDown(Phaser.Keyboard.LEFT) )
    {
    	player.body.velocity.x = -200;
        if( player.scale.x != -1)
        {
            player.scale.x = -1;
        }
        player.animations.play('walking');
    }
    	else if (game.input.keyboard.isDown(Phaser.Keyboard.RIGHT) )
    {
    	player.body.velocity.x = 200;


        if( player.scale.x != 1)
            player.scale.x = 1;
        player.animations.play('walking');

    } else 
    {
        player.body.velocity.x = 0;

        player.animations.stop();
    }

    if( game.input.keyboard.isDown(Phaser.Keyboard.SPACEBAR) && player.body.touching.down )
    {
    	player.body.velocity.y = -400;
    }


    //If the player overlaps with any gems call CollectedGem
    game.physics.arcade.overlap(player, gems, CollectedGem, null, this);
    game.physics.arcade.overlap(player, key, CollectedKey, null, null);

    playerDead = false;
    game.physics.arcade.overlap(player,water,KillPlayer,null,null);
}


function KillPlayer(player,waterTile)
{

    if( !playerDead )
    {
        health--;

        if( health < 0 )
        {
            levelIndex = 0;

            health = 3;
        }
        
        levels[levelIndex]();
        playerDead = true;
    }
}



function CollectedKey(player,key)
{

    levelIndex++;

    if( levelIndex == levels.length )
        levelIndex = 0;

    levels[levelIndex]();
}


function CollectedGem(player,gem)
{
    var numPlatforms = platforms.length;

    for(var index = 0; index < numPlatforms; ++index)
    {
        if( platforms[index].colour == gem.colour)
        {            
            platforms[index].ActivationFunction();
            platforms[index].Update = platforms[index].UpdateReplace;
        }
        else
        { 
            platforms[index].Update = Platform.prototype.Update;
            platforms[index].DeacticationFunction();
        }
    }

    gem.kill();
}


function render()
{


}